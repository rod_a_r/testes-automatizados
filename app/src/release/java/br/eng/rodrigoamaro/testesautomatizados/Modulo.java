package br.eng.rodrigoamaro.testesautomatizados;

import dagger.Module;
import dagger.Provides;

@Module
public class Modulo {

    @Provides
    public Servico proverServico() {
        return getServico();
    }


    protected Servico getServico() {
        return new ServicoImpl();
    }

}
