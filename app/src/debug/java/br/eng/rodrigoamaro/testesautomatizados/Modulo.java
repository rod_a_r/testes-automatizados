package br.eng.rodrigoamaro.testesautomatizados;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

@Module
public class Modulo {

    private static Servico sServico;

    public static void setServico(Servico servico) {
        sServico = servico;
    }

    @Provides
    public Api proverApi() {
        return getApi();
    }

    protected Api getApi() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BuildConfig.HOST)
                .build();
        return restAdapter.create(Api.class);
    }

    @Provides
    public Servico proverServico(Api api) {
        if (sServico == null) {
            sServico = getServico(api);
        }
        return sServico;
    }


    protected Servico getServico(Api api) {
        return new ServicoImpl(api);
    }
}
