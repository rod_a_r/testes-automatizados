package br.eng.rodrigoamaro.testesautomatizados;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import rx.Observable;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
@RunWith(AndroidJUnit4.class)
public class LoginTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class, true, false);

    private Servico mServico = mock(Servico.class);

    @Before
    public void setUp() {
        Modulo.setServico(mServico);
    }

    @OfflineTest
    @Test
    public void estamosNaTelaDeLogin() {
        mActivityRule.launchActivity(new Intent());

        onView(withText("Usuário")).perform(closeSoftKeyboard()).check(matches(isDisplayed()));
        onView(withId(R.id.edit_username)).check(matches(isDisplayed()));
        onView(withText("Senha")).check(matches(isDisplayed()));
        onView(withId(R.id.edit_password)).perform(scrollTo()).check(matches(isDisplayed()));
        onView(withText("Acessar")).perform(scrollTo()).check(matches(isDisplayed()));
    }

    @OfflineTest
    @Test
    public void loginComSucesso() throws ErroDeRede, UsuarioOuSenhaInvalidos {
        doReturn(Observable.just(mock(DetalhesDoUsuario.class)))
                .when(mServico)
                .validar(anyString(), anyString());
        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.edit_username)).perform(typeText("login@ok.com"), closeSoftKeyboard());
        onView(withId(R.id.edit_password)).perform(typeText("senha"), closeSoftKeyboard());
        onView(withText("Acessar")).perform(scrollTo(), click());
        onView(withText("Acesso concedido")).check(matches(isDisplayed()));
    }

    @OfflineTest
    @Test
    public void loginComUsuarioSenhaInvalidos() throws ErroDeRede, UsuarioOuSenhaInvalidos,
            InterruptedException {
        doReturn(Observable.error(new UsuarioOuSenhaInvalidos()))
                .when(mServico)
                .validar(anyString(), anyString());

        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.edit_username)).perform(typeText("login@falhou.com"), closeSoftKeyboard());
        onView(withId(R.id.edit_password)).perform(typeText("senha"), closeSoftKeyboard());
        onView(withText("Acessar")).perform(scrollTo(), click());
        onView(withText("Acesso negado")).check(matches(isDisplayed()));
    }

    @OfflineTest
    @Test
    public void loginComFalhaDeInternet() throws ErroDeRede, UsuarioOuSenhaInvalidos {
        doReturn(Observable.error(new ErroDeRede()))
                .when(mServico)
                .validar(anyString(), anyString());
        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.edit_username)).perform(typeText("login@seminternet.com"), closeSoftKeyboard());
        onView(withId(R.id.edit_password)).perform(typeText("senha"), closeSoftKeyboard());
        onView(withText("Acessar")).perform(scrollTo(), click());
        onView(withText("Sem conexão! Tentar novamente?")).check(matches(isDisplayed()));
    }

    @OnlineTest
    @Test
    public void loginComUsuarioSenhaInvalidosComAcessoRede() throws ErroDeRede,
            UsuarioOuSenhaInvalidos {
        Modulo.setServico(null);

        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.edit_username)).perform(typeText("login@falhou.com"), closeSoftKeyboard());
        onView(withId(R.id.edit_password)).perform(typeText("senha"), closeSoftKeyboard());
        onView(withText("Acessar")).perform(scrollTo(), click());
        onView(withText("Acesso negado")).check(matches(isDisplayed()));
    }

    @OnlineTest
    @Test
    public void loginComUsuarioSenhaInvalidosComAcessoRedeDemorado() throws ErroDeRede,
            UsuarioOuSenhaInvalidos {
        Modulo.setServico(null);

        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.edit_username)).perform(typeText("login.demorado@falhou.com"),
                closeSoftKeyboard());
        onView(withId(R.id.edit_password)).perform(typeText("senha"), closeSoftKeyboard());
        onView(withText("Acessar")).perform(scrollTo(), click());
        onView(withText("Acesso negado")).check(matches(isDisplayed()));
    }

    @OnlineTest
    @Test
    public void loginComSucessoComAcessoRede() throws ErroDeRede, UsuarioOuSenhaInvalidos {
        Modulo.setServico(null);

        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.edit_username)).perform(typeText("login@ok.com"), closeSoftKeyboard());
        onView(withId(R.id.edit_password)).perform(typeText("senha"), closeSoftKeyboard());
        onView(withText("Acessar")).perform(scrollTo(), click());
        onView(withText("Acesso concedido")).check(matches(isDisplayed()));
    }
}
