package br.eng.rodrigoamaro.testesautomatizados;

import android.os.Bundle;
import android.support.test.runner.AndroidJUnitRunner;

import rx.plugins.RxJavaPlugins;

public class CustomRunner extends AndroidJUnitRunner {
    @Override
    public void onCreate(Bundle arguments) {
        RxJavaPlugins.getInstance().registerObservableExecutionHook(RxIdlingResource.get());
        super.onCreate(arguments);
    }
}
