package br.eng.rodrigoamaro.testesautomatizados;


import retrofit.http.Body;
import retrofit.http.POST;
import rx.Observable;

public interface Api {

    @POST("/login")
    Observable<DetalhesDoUsuario> login(@Body Credenciais credenciais);

}
