package br.eng.rodrigoamaro.testesautomatizados;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private EditText mEditUsuario;
    private EditText mEditSenha;
    private Button mBotaoLogin;
    @Inject
    protected Servico mServico;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        localizarViews();
        tratamentoDeAcoes();
        Aplicacao.getInstance().injetor().inject(this);
    }

    private void tratamentoDeAcoes() {
        mBotaoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                realizarLogin();
            }
        });
    }

    private void realizarLogin() {
        String usuario = mEditUsuario.getText().toString();
        String senha = mEditSenha.getText().toString();
        Observable<DetalhesDoUsuario> observable = mServico.validar(usuario, senha)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        observable.subscribe(new Subscriber<DetalhesDoUsuario>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                if (e instanceof UsuarioOuSenhaInvalidos) {
                    mostrarUsuarioOuSenhaInvalidos();
                } else if (e instanceof ErroDeRede) {
                    verificarNovaTentativa();
                }
            }

            @Override
            public void onNext(DetalhesDoUsuario detalhesDoUsuario) {
                mostrarSucesso();
            }
        });
    }

    private void verificarNovaTentativa() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        AlertDialog alertDialog = builder
                .setTitle(R.string.mensagem_sem_internet)
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        realizarLogin();
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create();
        alertDialog.show();
    }

    private void mostrarUsuarioOuSenhaInvalidos() {
        exibirDialog(R.string.mensagem_login_incorreto);
    }

    private void mostrarSucesso() {
        exibirDialog(R.string.mensagem_login_sucesso);
    }

    private void exibirDialog(int mensagem) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        AlertDialog alertDialog = builder
                .setTitle(mensagem)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create();
        alertDialog.show();
    }

    private void localizarViews() {
        mEditUsuario = (EditText) findViewById(R.id.edit_username);
        mEditSenha = (EditText) findViewById(R.id.edit_password);
        mBotaoLogin = (Button) findViewById(R.id.btn_login);
    }
}
