package br.eng.rodrigoamaro.testesautomatizados;

import retrofit.RetrofitError;
import rx.Observable;
import rx.functions.Func1;

public class ServicoImpl implements Servico {

    private final Api mApi;

    public ServicoImpl(Api api) {
        mApi = api;
    }

    @Override
    public Observable<DetalhesDoUsuario> validar(String usuario, String senha) {
        return mApi.login(new Credenciais(usuario, senha))
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends DetalhesDoUsuario>>() {
                    @Override
                    public Observable<? extends DetalhesDoUsuario> call(Throwable throwable) {
                        if (throwable instanceof RetrofitError) {
                            if (((RetrofitError) throwable).getResponse().getStatus() == 401) {
                                return Observable.error(new UsuarioOuSenhaInvalidos());
                            }
                        }
                        return Observable.error(throwable);
                    }
                });
    }
}
