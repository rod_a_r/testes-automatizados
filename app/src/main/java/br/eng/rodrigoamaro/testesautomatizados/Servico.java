package br.eng.rodrigoamaro.testesautomatizados;

import rx.Observable;

public interface Servico {

    Observable<DetalhesDoUsuario> validar(String usuario, String senha);

}
