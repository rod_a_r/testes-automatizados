package br.eng.rodrigoamaro.testesautomatizados;

public class Credenciais {

    protected final String usuario;

    protected final String senha;

    public Credenciais(String usuario, String senha) {
        this.usuario = usuario;
        this.senha = senha;
    }

}
