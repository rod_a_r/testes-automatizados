package br.eng.rodrigoamaro.testesautomatizados;

import dagger.Component;

@Component(modules = {Modulo.class})
public interface Injetor {
    void inject(MainActivity activity);
}
