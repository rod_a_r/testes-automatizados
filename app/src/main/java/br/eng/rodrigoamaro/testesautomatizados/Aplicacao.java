package br.eng.rodrigoamaro.testesautomatizados;

import android.app.Application;

public class Aplicacao extends Application {
    private static Aplicacao sInstance;
    private Injetor mInjetor;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        mInjetor = DaggerInjetor.builder().modulo(new Modulo()).build();
    }

    public static Aplicacao getInstance() {
        return sInstance;
    }

    public Injetor injetor() {
        return mInjetor;
    }
}
